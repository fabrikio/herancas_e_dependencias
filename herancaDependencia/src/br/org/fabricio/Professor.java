/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.fabricio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Aluno
 */
public class Professor extends Funcionario{
    String disciplina;

    public Professor(String nome, String endereco, int idade, double salario, String disciplina) {
        super(nome, endereco, idade, salario);
        this.disciplina=disciplina;
    }

    public String getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(String disciplina) {
        this.disciplina = disciplina;
    }

    //void getDisciplina(String text) {
    //    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    //}

    //void getSalario(Integer valueOf) {
    //    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    //}

    @Override
    public String getFuncao() {
       return "Professor";
        
    }
    
    /**
     *
     * @throws SQLException
     */
    @Override
    public void inserir(){
        Conexao c= new Conexao();
        Connection dbConnection = null;
        try {
            dbConnection = c.getConexao();
        } catch (SQLException ex) {
            Logger.getLogger(Professor.class.getName()).log(Level.SEVERE, null, ex);
        }
        PreparedStatement preparedStatement = null;
       String insertTableSQL = "INSERT INTO OO_Pessoa (nome,endereco,idade,salario,disciplina,tipo) VALUES (?,?,?,?,?,?)";
        try{
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setString(1,nome);
            preparedStatement.setString(2, endereco);
            preparedStatement.setInt(3,idade);
            preparedStatement.setDouble(4,salario);
            preparedStatement.setString(5,disciplina);
            preparedStatement.setString(6,"Professor"); 
            preparedStatement.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

}

   

   
