/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.fabricio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Aluno
 */
public class FuncAdm extends Funcionario{
    String setor;
    String funcao;

    public FuncAdm(String nome, String endereco, int idade, double salario, String setor, String funcao){
        super(nome, endereco, idade, salario);
        this.setor=setor;
        this.funcao=funcao;
        
    }

    public String getSetor() {
        return setor;
    }

    public void setSetor(String setor) {
        this.setor = setor;
    }

    /*public String getFuncao() {
        return funcao;
    }*/
    

    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }
    
    @Override
    public String getFuncao() {
       return "Funcionário";
               
        
    }

    @Override
    public void inserir() {
         Conexao c= new Conexao();
        Connection dbConnection = null;
        try {
            dbConnection = c.getConexao();
        } catch (SQLException ex) {
            Logger.getLogger(Professor.class.getName()).log(Level.SEVERE, null, ex);
        }
        PreparedStatement preparedStatement = null;
       String insertTableSQL = "INSERT INTO OO_Pessoa (nome,endereco,idade,salario,setor,funcao,tipo) VALUES (?,?,?,?,?,?,?)";
        try{
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setString(1,nome);
            preparedStatement.setString(2, endereco);
            preparedStatement.setInt(3,idade);
            preparedStatement.setDouble(4,salario);
            preparedStatement.setString(5,setor);
            preparedStatement.setString(6,funcao); 
            preparedStatement.setString(7,"Funcionário");
            preparedStatement.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
      
    }
   

}
