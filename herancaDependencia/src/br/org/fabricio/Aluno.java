/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.fabricio;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Aluno extends Pessoa{
    String semestre;
    String curso;

    public Aluno(String nome, String endereco, int idade, String semestre, String curso){
    super(nome,endereco,idade);
    this.semestre=semestre;
    this.curso=curso;
    }
    public String getSemestre() {
        return semestre;
    }

    public void setSemestre(String semestre) {
        this.semestre = semestre;
    }

    public String getCurso() {
        return curso;
    }

    public void setCurso(String curso) {
        this.curso = curso;
    }

    void getCurso(String text) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    void getSemestre(String text) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public String getFuncao() {
       return "Aluno";
        
    }
    
    @Override
    public void inserir() {
        Conexao c= new Conexao();
        Connection dbConnection = null;
        try {
            dbConnection = c.getConexao();
        } catch (SQLException ex) {
            Logger.getLogger(Aluno.class.getName()).log(Level.SEVERE, null, ex);
        }
        PreparedStatement preparedStatement = null;
       String insertTableSQL = "INSERT INTO OO_Pessoa (nome,endereco,idade,curso,semestre,tipo) VALUES (?,?,?,?,?,?)";
        try{
            preparedStatement = dbConnection.prepareStatement(insertTableSQL);
            preparedStatement.setString(1,nome);
            preparedStatement.setString(2, endereco);
            preparedStatement.setInt(3,idade);
            preparedStatement.setString(4,curso);
            preparedStatement.setString(5,semestre);
            preparedStatement.setString(6,"Aluno"); // mudar de acordo com a classe
            preparedStatement.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    
}
