package br.org.fabricio;

public abstract class Pessoa {
    String nome;
    int idade;
    String endereco;

    public Pessoa(String nome, String endereco, int idade){
        this.nome=nome;
        this.endereco=endereco;
        this.idade=idade;
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }
    
    public abstract String getFuncao();  
    
    public abstract void inserir();
}
