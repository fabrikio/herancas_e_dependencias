/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.fabricio;

/**
 *
 * @author Aluno
 */
public abstract class Funcionario extends Pessoa{
    double salario;

    public Funcionario(String nome, String endereco, int idade, double salario) {
        super(nome, endereco, idade);
        this.salario=salario;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    
}
