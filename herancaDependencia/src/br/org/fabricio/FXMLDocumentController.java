/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.org.fabricio;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;

/**
 * FXML Controller class
 *
 * @author Aluno
 */
public class FXMLDocumentController implements Initializable {

    private Pessoa b;
    @FXML
    private TextField nome_txt;
    @FXML
    private TextField idade_txt;
    @FXML
    private TextField endereco_txt;
    @FXML
    private RadioButton aluno_radio;
    @FXML
    private ToggleGroup tipo;
    @FXML
    private RadioButton professor_radio;
    @FXML
    private RadioButton funcadm_radio;
    @FXML
    private AnchorPane aluno_anchor;
    @FXML
    private TextField curso_txt;
    @FXML
    private TextField semestre_txt;
    @FXML
    private AnchorPane professor_anchor;
    @FXML
    private TextField salprof_txt;
    @FXML
    private TextField disciplina_txt;
    @FXML
    private AnchorPane funcadm_anchor;
    @FXML
    private TextField salfunc_txt;
    @FXML
    private TextField setor_txt;
    @FXML
    private TextField funcao_txt;
    @FXML
    private TableView<Pessoa> pessoa_tableview;
    @FXML
    private TableColumn<?, ?> nome_col;
    @FXML
    private TableColumn<?, ?> idade_col;
    @FXML
    private TableColumn<?, ?> endereco_col;
    @FXML
    private TableColumn<?, ?> funcao_col;
    @FXML
    private Button cadastrar_bt;
    @FXML
    private Button editar_bt;
    @FXML
    private Button excluir_bt;
    @FXML
    private Label aviso;
    
    private ObservableList<Pessoa> lista_pessoa;
    
    /**@FXML
    private AnchorPane AlunoDetAnchor;
    @FXML
    private Label cursoLabel;
    @FXML
    private Label semestreLabel;
    @FXML
    private AnchorPane FuncAdmAnchor;
    @FXML
    private Label setorLabel;
    @FXML
    private Label SalFuncLabel;
    @FXML
    private Label FuncaoLabel;
    @FXML
    private AnchorPane ProfDetLabel;
    @FXML
    private Label SalProfLabel;
    @FXML
    private Label DisciplinaLabel;*/
    

    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        lista_pessoa = pessoa_tableview.getItems();
        
        nome_col.setCellValueFactory(new PropertyValueFactory<>("nome"));
        idade_col.setCellValueFactory(new PropertyValueFactory<>("idade"));  
        endereco_col.setCellValueFactory(new PropertyValueFactory<>("endereco"));     
        funcao_col.setCellValueFactory(new PropertyValueFactory<>("funcao"));
        
        Pessoa f= new Aluno("Fabricio","Fatima",17,"3","DS");
        Pessoa m= new Professor("Mannu","Marechal",17,3000,"Ingles");
        Pessoa d= new FuncAdm("Douglas","Niteroi",18,12000,"Admnistração","Diretor");
        pessoa_tableview.getItems().add(f);
        pessoa_tableview.getItems().add(m);
        pessoa_tableview.getItems().add(d);
    } 
    
    @FXML
    public void cadastra(ActionEvent event){
        Pessoa p=null;
        
        if(aluno_radio.isSelected()){
            //Aluno a = (Aluno) p;
            Aluno a = new Aluno(nome_txt.getText(),endereco_txt.getText(),Integer.valueOf(idade_txt.getText()),semestre_txt.getText(),curso_txt.getText());
            a.setCurso(curso_txt.getText());
            a.setSemestre(semestre_txt.getText());
            p=a;
        }else if(professor_radio.isSelected()){
           //Professor pr = (Professor) p; 
           Professor pr = new Professor(nome_txt.getText(),endereco_txt.getText(),Integer.valueOf(idade_txt.getText()),Double.valueOf(salprof_txt.getText()),disciplina_txt.getText());
           pr.setSalario(Double.valueOf(salprof_txt.getText()));
           pr.setDisciplina(disciplina_txt.getText());
           p=pr;
        }else if(funcadm_radio.isSelected()){
            //FuncAdm fc = (FuncAdm) fc;
            FuncAdm fc = new FuncAdm(nome_txt.getText(),endereco_txt.getText(),Integer.valueOf(idade_txt.getText()),Double.valueOf(salfunc_txt.getText()), setor_txt.getText(), funcao_txt.getText());
            fc.setSalario(Double.valueOf(salfunc_txt.getText()));
            fc.setSetor(setor_txt.getText());
            fc.setFuncao(funcao_txt.getText());
            p=fc;
        }
        p.setIdade(Integer.valueOf(idade_txt.getText()));
        p.setNome(nome_txt.getText());
        p.setEndereco(endereco_txt.getText());
        pessoa_tableview.getItems().add(p);
        nome_txt.clear();
        idade_txt.clear();
        endereco_txt.clear();
        curso_txt.clear();
        semestre_txt.clear();
        disciplina_txt.clear();
        salprof_txt.clear();
        salfunc_txt.clear();
        funcao_txt.clear();
        setor_txt.clear();
        
   }

    
    public void edita(){
        Pessoa p = pessoa_tableview.getSelectionModel().getSelectedItem();
        pessoa_tableview.getItems().remove(p);
        /*p.setIdade(Integer.valueOf(idade_txt.getText()));
        p.setNome(nome_txt.getText());
        p.setEndereco(endereco_txt.getText());*/
        nome_txt.setText(p.getNome());
        idade_txt.setText(""+p.getIdade());
        endereco_txt.setText(p.getEndereco());
        if( p instanceof Aluno){
            Aluno a=(Aluno)p;
            aluno_radio();
            semestre_txt.setText(a.getSemestre());
            curso_txt.setText(a.getCurso());
            aluno_radio.setSelected(true);
        }else if(p instanceof Professor){
            Professor pr=(Professor)p;
            professor_radio();
            salprof_txt.setText(""+pr.getSalario());
            disciplina_txt.setText(pr.getDisciplina());
            professor_radio.setSelected(true);
        }else if (p instanceof FuncAdm){
            FuncAdm fc=(FuncAdm)p;
            funcadm_radio();
            salfunc_txt.setText(""+fc.getSalario());
            funcao_txt.setText(fc.getFuncao());
            funcadm_radio.setSelected(true);
        }
        if(p.getFuncao()=="Funcionario"){
    
    }

    }
    
    public void deleta(){
    Pessoa p=pessoa_tableview.getSelectionModel().getSelectedItem();
    pessoa_tableview.getItems().remove(p);
    }

    @FXML
    private void aluno_radio() {
    aluno_anchor.setVisible(true);
    professor_anchor.setVisible(false);
    funcadm_anchor.setVisible(false);
    }

    @FXML
    private void professor_radio() {
    professor_anchor.setVisible(true);
    aluno_anchor.setVisible(false);
    funcadm_anchor.setVisible(false);
    }

    @FXML
    private void funcadm_radio() {
    funcadm_anchor.setVisible(true);
    aluno_anchor.setVisible(false);
    professor_anchor.setVisible(false);
    }
    
    
    }




